var scene = new PIXI.Container(),
    renderer = PIXI.autoDetectRenderer(500, 500);
document.body.appendChild(renderer.view);

//load images to loader.resources
PIXI.loader
    .add("images/ryu.png")
    .load(setup);

function setup() {
    var testRyuSprite = new PIXI.Sprite(
        PIXI.loader.resources["images/ryu.png"].texture
    );

    //adding loaded sprite to scene
    scene.addChild(testRyuSprite);

    renderer.render(scene);
}
