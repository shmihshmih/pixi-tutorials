var Container = PIXI.Container,
    Sprite = PIXI.Sprite,
    Renderer = PIXI.autoDetectRenderer,
    Loader = PIXI.loader,
    Resources = PIXI.loader.resources;

var container = new Container(),
    renderer = new Renderer(512, 512);

var state = play,explorer,
    blob = [];
document.body.appendChild(renderer.view);

Loader
    .add('images/treasureHunter.json')
    .on('progress', loadProgressHandler)
    .load(setup);

function setup() {
    console.log('setup');
    var treasure, door, dungeon,
        spritesArr = Resources['images/treasureHunter.json'].textures;

    /* dungeon */
    dungeon = new Sprite(spritesArr['dungeon.png']);
    container.addChild(dungeon);

    /* explorer */
    explorer = new Sprite(spritesArr['explorer.png']);
    explorer.x = 68;
    explorer.y = container.height / 2 - explorer.height / 2;
    container.addChild(explorer);

    /* door */
    door = new Sprite(spritesArr['door.png']);
    door.x = door.width;
    container.addChild(door);

    /* treasure */
    treasure = new Sprite(spritesArr['treasure.png']);
    treasure.x = container.width  - treasure.width - 48;
    treasure.y = container.height / 2 - treasure.height / 2;
    container.addChild(treasure);

    /* blobs 6 */
    for(var i = 0; i < 6; i++) {
        this.blob[i] = new Sprite(spritesArr['blob.png']);
        this.blob[i].vx = this.randomInt(-1, 1);
        this.blob[i].vy = this.randomInt(-1, 1);
        this.blob[i].x = this.randomInt(this.blob[i].width, container.width - this.blob[i].width*2);
        this.blob[i].y = this.randomInt(this.blob[i].height, container.height - this.blob[i].height*2);
        container.addChild(this.blob[i]);
    }

    /* keyboard control */
    var up = keyboard(119),
        down = keyboard(115),
        left = keyboard(97),
        right = keyboard(100);
//
    up.press = function() {
        explorer.vx = 0;
        explorer.vy = -5;
    };
    up.release = function() {
        if(down.isDown && explorer.vx === 0) {
            explorer.vy = 0;
        }
    };
//
    down.press = function() {
        explerer.vy = -5;
        explorer.vx = 0;
    };
    down.release = function() {
        if(!up.isDown && explorer.vx === 0) {
            explorer.vy = 0;
        }
    };
//
    left.press = function() {
        explorer.vx = -5;
        explorer.vy= 0;
    };
    left.release = function() {
        if(!right.isDown && explorer.vy === 0) {
            explorer.vx = 0;
        }
    };
//
    right.press = function() {
        console.log('right');
        explorer.vx = 5;
        explorer.vy = 0;
    };
    right.release = function() {
        console.log('right');
        if(!left.isDown && explorer.vy === 0) {
            explorer.vx = 0;
        }
    };

    /* LOOPS */
    gameLoop();

}

function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min +1)) + min;
}

function gameLoop() {
    requestAnimationFrame(gameLoop);
    state();
    renderer.render(container);
}

function keyboard(keycode) {
    var key = {};
    key.code = keycode;
    key.isDown = false;
    key.isUp = true;
    key.press = undefined;
    key.release = undefined;
    key.downHandler = function(event) {
        console.log("there function of down");
        if(event.keycode === key.code) {
            if(key.isUp && key.press) {
                key.press();
            };
            key.isDown = true;
            key.isUp = false;
        }
        event.preventDefault();
    };
    key.upHandler = function(event) {
        if(event.keycode === key.code) {
            if(key.isDown && key.release) {
                key.release();
            };
            key.isDown = false;
            key.isUp = true;
        }
        event.preventDefault();
    };
    window.addEventListener(
        'keydown', key.downHandler.bind(key), false
    );
    window.addEventListener(
        'keyup', key.upHandler.bind(key), false
    );
    return key;
}

function play() {
    explorer.x += explorer.vx;
    explorer.y += explorer.vy;
    
    /* устанавливаем все блобам движение и меняем направление, когда достигают максимума */
    for(var i = 0; i < this.blob.length; i++) {
        this.blob[i].x += this.blob[i].vx;
        this.blob[i].y += this.blob[i].vy;

        if((this.blob[i].x >= (container.width - this.blob[i].width*2)) || (this.blob[i].x === this.blob[i].width)) {
            this.blob[i].vx *= (-1);
        }
        if((this.blob[i].y >= (container.height - this.blob[i].height*2)) || (this.blob[i].y === this.blob[i].height)) {
            this.blob[i].vy *= (-1);
        }
    }
}

function loadProgressHandler(loader, resource) {
  //Display the file `url` currently being loaded
  console.log("loading: " + resource.url); 

  //Display the precentage of files currently loaded
  console.log("progress: " + loader.progress + "%"); 

  //If you gave your files names as the first argument 
  //of the `add` method, you can access them like this
  //console.log("loading: " + resource.name);
}