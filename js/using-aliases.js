var Container = PIXI.Container,
    Renderer = PIXI.autoDetectRenderer,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    Sprite = PIXI.Sprite;

var container = new Container(),
    renderer = Renderer(300,300);
document.body.appendChild(renderer.view);

loader
    .add('first', 'images/ryu.png')
    .add({name:'second',url:'images/ryu.png'}, function() {console.log('on complete second')})
    .add({name:'third', url:'images/ryu.png',  onComplete: function() {console.log('on complete 3')}})
    .on("progress", loadProgressHandler)
    .load(setup);

function setup() {
    var Fighter = new Sprite(resources["second"].texture);
    container.addChild(Fighter);
    renderer.render(container);
}

function loadProgressHandler(loader, resource) {
    console.log(loader);
    console.log(resource);
    //Display the file `url` currently being loaded
    console.log("loading: " + resource.url);

    //Display the precentage of files currently loaded
    console.log("progress: " + loader.progress + "%");
}