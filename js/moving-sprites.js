var Container = PIXI.Container,
    Renderer = PIXI.autoDetectRenderer,
    Sprite = PIXI.Sprite,
    Resources = PIXI.loader.resources,
    //TextureCache = PIXI.utils.TextureCache,
    Loader = PIXI.loader;

container = new Container;
renderer = new Renderer(512, 512);
document.body.appendChild(renderer.view);

Loader
    .add('images/treasureHunter.json')
    .load(setup);

//moving blob
var blob;

function setup() {
    var dungeon, explorer, door, treasure, id;

    id = Resources['images/treasureHunter.json'].textures;

    dungeon = new Sprite(id['dungeon.png']);
    container.addChild(dungeon);

    explorer = new Sprite(id['explorer.png']);
    explorer.x = explorer.width*3;
    explorer.y = container.height / 2 - explorer.height / 2;
    container.addChild(explorer);

    door = new Sprite(id['door.png']);
    door.x = door.width;
    container.addChild(door);

    treasure = new Sprite(id['treasure.png']);
    treasure.x = container.width - treasure.width*3;
    treasure.y = container.height / 2 - treasure.height / 2;
    container.addChild(treasure);

    for (var i = 0; i < 6; i++) {
        blob = new Sprite(id['blob.png']);
        blob.x = this.intRandom(0, container.width - blob.width);
        blob.y = this.intRandom(0, container.height - blob.height);
        container.addChild(blob);
    }
    this.gameLoop();
}

function intRandom(min, max) {
    return Math.floor(Math.random()*(max-min+1))+min;
}

  //valocity of moving (blob.vx)
  var vx = intRandom(0,22);

function gameLoop() {

    //recursive fnc to move
    requestAnimationFrame(gameLoop);

    state();

    renderer.render(container);
}

var state = play;

function play() {
    blob.x += vx;
}