console.log('hello arthur!');
var Container = PIXI.Container,
    Renderer = PIXI.autoDetectRenderer,
    resources = PIXI.loader.resources,
    Sprite = PIXI.Sprite,
    Loader = PIXI.loader;
var TextureCache = PIXI.utils.TextureCache;

var container = new Container(),
    renderer = Renderer(300, 300);
document.body.appendChild(renderer.view);

Loader
    .add({name:'tile',
          url:'images/water_64x64.png'})
    .load(setup);

function setup() {
    var tile = TextureCache["tile"];

    var water_part = new PIXI.Rectangle(32,32,32,32);

    tile.frame = water_part;

    var water = new Sprite(tile);

    water.position.set(50,50);

    container.addChild(water);
    renderer.render(container);
}
