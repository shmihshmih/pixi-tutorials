var Container = PIXI.Container,
    Renderer = PIXI.autoDetectRenderer,
    Sprite = PIXI.Sprite,
    Resources = PIXI.loader.resources,
    TextureCache = PIXI.utils.TextureCache,
    Loader = PIXI.loader;

var container = new Container,
    renderer = Renderer(512, 512);

document.body.appendChild(renderer.view);

Loader
    .add('images/treasureHunter.json')
    .load(setup);

function setup() {
    console.log(this);
    var dungeon, explorer, blob, treasure, id, door;

    //1st type of add sprites from atlas
    var dungeonTexture = TextureCache['dungeon.png'];
    dungeon = new Sprite(dungeonTexture);
    container.addChild(dungeon);

    //2nd type of add sprites from atlas
    explorer = new Sprite(Resources['images/treasureHunter.json'].textures['explorer.png']);
    explorer.x = 68;
    explorer.y = container.height / 2 - explorer.height / 2;
    container.addChild(explorer);

    //3rd type of add sprites from atlas
    id = Resources['images/treasureHunter.json'].textures;
    treasure = new Sprite(id['treasure.png']);
    container.addChild(treasure);

    door = new Sprite(id['door.png']);
    door.x = door.width;
    container.addChild(door);

    for (var i=0; i<5;i++) {
        blob = new Sprite(id['blob.png']);
        blob.x = this.randomInt(0, container.width - blob.width);
        blob.y = this.randomInt(0, container.height - blob.height);
        container.addChild(blob);
    }

    treasure.x = container.width - treasure.width - 48;
    treasure.y = container.height / 2 - treasure.height / 2;
    container.addChild(treasure);

    renderer.render(container);
}

function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}