//Create the renderer
renderer = PIXI.autoDetectRenderer(
    256, 256,
    {antialias: false, transparent: false, resolution: 1}
);
//change style for this
renderer.view.style.border = "1px dashed black";

//change background for this
renderer.backgroundColor = 0x061639;

//resizing to whole window
renderer.view.style.position = "absolute";
renderer.view.style.display = "block";
renderer.autoResize = true;
renderer.resize(window.innerWidth, window.innerHeight);

//change size of renderer
renderer.autoResize = true;
renderer.resize(512, 512);

//Add the canvas to the HTML document
document.body.appendChild(renderer.view);

//Create a container object called the `stage`
var scene = new PIXI.Container();

//Tell the `renderer` to `render` the `stage`
renderer.render(scene);