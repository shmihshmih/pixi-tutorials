var Container = PIXI.Container,
    Scene = PIXI.autoDetectRenderer,
    Sprite = PIXI.Sprite,
    resources = PIXI.loader.resources,
    Loader = PIXI.loader;

var container = new Container(),
    renderer = Scene(300,300);
document.body.appendChild(renderer.view);

Loader
    .add('images/ryu.png')
    .load(setup);

function setup() {
    var Fighter = new Sprite(resources['images/ryu.png'].texture);
    //move sprite
    Fighter.x = 50;
    Fighter.y = 50;
    container.addChild(Fighter);
    renderer.render(container);

    //change position and rerender
    setTimeout(function() {
        console.log('after 2 s');
        //x and y
        Fighter.position.set(10, 10);
        //size
        Fighter.width  = 50;
        Fighter.height = 50;
        renderer.render(container);
    }, 2000);

    //scale
    setTimeout(function() {
        console.log('after 4 s');
        Fighter.scale.x = 1;
        Fighter.scale.y = 1;
        //cat.scale.set(0.5, 0.5);
        renderer.render(container);
    }, 4000);

    //rotation
    setTimeout(function() {
        console.log('after 6 s');
        Fighter.rotation = 0.5;
        renderer.render(container);
    }, 6000);

    //anchor changing
    setTimeout(function() {
        console.log('after 7 s');
        Fighter.anchor.x = 0.5;
        Fighter.anchor.y = 0.5;
        Fighter.rotation = 0.5;
        //cat.anchor.set(0.5, 0.5);
        renderer.render(container);
    }, 7000);

    //pivot changing
    setTimeout(function() {
        console.log('after 8 s');
        Fighter.pivot.x = 25;
        Fighter.pivot.y = 25;
        Fighter.rotation = 0.5;
        //cat.pivot.set(0.5, 0.5);
        renderer.render(container);
    }, 8000);
}

